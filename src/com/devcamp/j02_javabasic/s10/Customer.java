package com.devcamp.j02_javabasic.s10;

public class Customer {
    int myNum; // integer (whole number)
    float myFloatNum; // floating point number
    char myLetter; // character
    boolean myBool; // boolean
    String myName; // String

    public Customer() {
        myNum = 5; // integer (whole number)
        myFloatNum = 5.99f; // floating point number
        myLetter = 'H'; // character
        myBool = true; // boolean
        myName = "HieuHN"; // String
    }

    public Customer(int num, float floatNum, char letter, boolean bool, String name) {
        myNum = num;
        myFloatNum = floatNum;
        myLetter = letter;
        myBool = bool;
        myName = name;
    }

    public void ShowData(){
        System.out.println("Cách println khác");
        System.out.println(this.myNum);
        System.out.println(this.myFloatNum);
        System.out.println(this.myLetter);
        System.out.println(this.myBool);
        System.out.println(this.myName);
    }

    public static void main(String[] args) throws Exception {
        Customer customer = new Customer();
        //cách println khác
        customer.ShowData();
        System.out.println(customer.myNum);
        System.out.println(customer.myFloatNum);
        System.out.println(customer.myLetter);
        System.out.println(customer.myBool);
        System.out.println(customer.myName);

        customer = new Customer(1, 5.88f, 'G', false, "Devcamp");// new object with pramater
        System.out.println("new object with pramater");
        System.out.println(customer.myNum);
        System.out.println(customer.myFloatNum);
        System.out.println(customer.myLetter);
        System.out.println(customer.myBool);
        System.out.println(customer.myName);
    }
}
